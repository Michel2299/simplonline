
				"Requêtes d'interrogation simples."

1."Afficher le numéro, le nom, le prénom, le genre et la date de naissance de chaque employé. ( emp_no, last_name, first_name,  gender, birth_date )"

select emp_no, last_name, first_name, gender, birth_date from employees;


2."Trouver tous les employés dont le prénom est 'Troy'. ( emp_no, first_name, last_name, gender )"

select emp_no, first_name, last_name, gender from employees where first_name like 'Troy';

3."Trouver tous les employés de sexe féminin ( * )."

select * from employees where gender = 'F';

4."Trouver tous les employés de sexe masculin nés après le 31 janvier 1965 ( * ) ?"

select * from employees where gender = 'M' AND birth_date > '1965-01-31';

5."Lister uniquement les titres des employés, sans que les valeurs apparaissent plusieurs fois. (title) ?"

select DISTINCT title from titles;

6."Combien y a t'il de département ? ( nombreDep ) ?"

select count (dept_no) as nombreDep from departments;

7."Qui a eu le salaire maximum de tous les temps, et quel est le montant de ce salaire ? (emp_no, maxSalary) ?"

SELECT first_name, salary FROM salaries INNER JOIN employees ON salaries.emp_no = employees.emp_no WHERE salary = (SELECT MAX(salary) as maxSalary from salaries);

8."Quel est salaire moyen de l'employé numéro 287323 toute période confondue ?  (emp_no, salaireMoy) ?"

SELECT AVG(salary) from salaries where emp_no = 287323;

9."Qui sont les employés dont le prénom fini par 'ard' (*) ?"

select first_name from employees where first_name like '%ard';

10."Combien de personnes dont le prénom est 'Richard' sont des femmes ?"

select * from employees where first_name like '%ard' and gender = 'F';

11."Combien y a t'il de titre différents d'employés (nombreTitre) ?"

SELECT COUNT ( DISTINCT title) from titles;

12."Dans combien de département différents a travaillé l'employé numéro 287323 (nombreDep) ?"

SELECT COUNT(departments.dept_no) as nombreDep from dept_emp INNER JOIN departments ON dept_emp.dept_no = departments.dept_no where emp_no = 287323;

13."Quels sont les employés qui ont été embauchés en janvier 2000. (*) ? Les résultats doivent être ordonnés par date d'embauche chronologique"

SELECT * from employees where hire_date BETWEEN '2000-01-01' AND '2000-01-31' order by hire_date ASC;

14."Quelle est la somme cumulée des salaires de toute la société (sommeSalaireTotale) ?"

SELECT SUM(salary) as sommeSalaireTotale FROM salaries;












			"Requêtes avec jointures :"

15."Quel était le titre de Danny Rando le 12 janvier 1990 ? (emp_no, first_name, last_name, title) ?"

SELECT title, first_name, last_name, from_date, to_date from titles INNER JOIN employees ON titles.emp_no = employees.emp_no WHERE first_name like 'Danny' and last_name like 'Rando' and from_date < '1990-01-12' and to_date > '1990-01-12';

16"Combien d'employés travaillaient dans le département 'Sales' le 1er Janvier 2000 (nbEmp) ?"

SELECT count(emp_no) as nbEmp from departments inner join dept_emp on departments.dept_no = dept_emp.dept_no where dept_name like 'Sales' and from_date < '2000-01-01' and to_date > '2000-01-01';

17."Quelle est la somme cumulée des salaires de tous les employés dont le prénom est Richard (emp_no, first_name, last_name, sommeSalaire) ?"

SELECT SUM(salary) as sommeSalaire from salaries inner join employees on salaries.emp_no = employees.emp_no WHERE first_name like 'Richard';










			"Agrégation"

19."Indiquer pour chaque prénom 'Richard', 'Leandro', 'Lena', le nombre de chaque genre (first_name, gender, nombre).
 Les résultats seront ordonnés par prénom décroissant et genre."

SELECT first_name,gender, count(gender) AS nombre  FROM employees WHERE first_name LIKE 'Richard' OR first_name like 'Leandro' OR first_name LIKE 'Lena' GROUP BY employees.first_name, gender ORDER BY nombre desc;

20."Quels sont les noms de familles qui apparaissent plus de 200 fois (last_name, nombre) ? Les résultats seront triés par leur nombre croissant et le nom de famille."

SELECT last_name, count(last_name) as nombre from employees GROUP BY last_name Having count(last_name)>200  ORDER BY nombre ASC;


21."Qui sont les employés dont le prénom est Richard qui ont gagné en somme cumulée plus de 1 000 000 (emp_no, first_name, last_name, hire_date, sommeSalaire) ?"

SELECT employees.emp_no,first_name, last_name, hire_date, SUM(salary) as sommeSalaire from employees INNER JOIN salaries ON employees.emp_no = salaries.emp_no WHERE  first_name like 'Richard' 
GROUP BY employees.emp_no Having sum(salary) > 1000000;

22."Quel est le numéro, nom, prénom  de l'employé qui a eu le salaire maximum de tous les temps, et quel est le montant de ce salaire ? (emp_no, first_name, last_name, title, maxSalary)"


SELECT employees.emp_no, last_name, first_name, salary , title FROM employees INNER JOIN salaries ON employees.emp_no = salaries.emp_no INNER JOIN titles ON titles.emp_no = salaries.emp_no 
WHERE salary = (SELECT MAX(salary) as maxSalary from salaries);











"bonus. Qui est le manager de Martine Hambrick actuellement et quel est son titre (emp_no, first_name, last_name, title)"

SELECT employees.first_name, employees.last_name, dept_name, employees.emp_no, hire_date from employees 
INNER JOIN dept_emp ON employees.emp_no = dept_emp.emp_no
INNER JOIN departments ON dept_emp.dept_no = departments.dept_no
GROUP BY employees.first_name, employees.last_name, dept_name, employees.emp_no, hire_date
Having first_name like 'Martine' and last_name like 'Hambrick';

emp_no = 280872
hire_date : 1986-10-29
dept_name: Finance


SELECT dept_name,first_name,last_name,title from departments INNER JOIN dept_manager ON departments.dept_no = dept_manager.dept_no
INNER JOIN employees ON dept_manager.emp_no = employees.emp_no
INNER JOIN titles ON dept_manager.emp_no = titles.emp_no
WHERE title like 'Manager' AND dept_name like 'Finance' AND dept_manager.from_date >= '1986-10-29';

ISAMU LEGLEITNER MANAGER









			"LA SUITE"




23."Quel est actuellement le salaire moyen de chaque titre  (title, salaireMoyen) ? Classé par salaireMoyen croissant"

SELECT title, AVG(salary) AS salaireMoyen from salaries INNER JOIN titles ON salaries.emp_no = titles.emp_no GROUP BY title ORDER BY AVG(salary) ASC;

24."Combien de manager différents ont eu les différents départements (dept_no, dept_name, nbManagers), Classé par nom de département"

SELECT departments.dept_no,dept_name,COUNT(title) AS nbManager from departments INNER JOIN dept_manager ON departments.dept_no = dept_manager.dept_no
INNER JOIN titles ON dept_manager.emp_no = titles.emp_no
 WHERE title like 'Manager' GROUP BY departments.dept_no, dept_name ORDER BY dept_name;

25."Quel est le département de la société qui a le salaire moyen le plus élevé (dept_no, dept_name, salaireMoyen)"

SELECT departments.dept_no, dept_name, AVG(salary) AS salaireMoyen from departments INNER JOIN dept_emp ON departments.dept_no = dept_emp.dept_no
INNER JOIN salaries ON dept_emp.emp_no = salaries.emp_no GROUP BY departments.dept_no, dept_name ORDER BY AVG(salary) DESC LIMIT 1;


26."Quels sont les employés qui ont eu le titre de 'Senior Staff' sans avoir le titre de 'Staff' ( emp_no , birth_date , first_name , last_name , gender , hire_date )"

SELECT employees.emp_no, birth_date, title, first_name, last_name, gender,hire_date,from_date, to_date, count(title) as count, SUM(COUNT(title)) OVER() AS total_count 
FROM employees 
INNER JOIN titles ON employees.emp_no = titles.emp_no
WHERE hire_date = titles.from_date
GROUP BY title, employees.emp_no,birth_date,first_name,last_name,gender, hire_date, from_date, to_date
Having title = 'Senior Staff';

27."Indiquer le titre et le salaire de chaque employé lors de leur embauche (emp_no, first_name, last_name, title, salary)"


SELECT employees.emp_no, first_name, last_name, title, salary from employees INNER JOIN salaries ON employees.emp_no = salaries.emp_no
INNER JOIN titles ON employees.emp_no = titles.emp_no
WHERE hire_date = titles.from_date OR hire_date = salaries.from_date;

28."Quels sont les employés dont le salaire a baissé (emp_no, first_name, last_name)"

SELECT employees.emp_no, first_name, last_name, salary, from_date, to_date, hire_date from employees 
INNER JOIN salaries ON employees.emp_no = salaries.emp_no;

PAS REUSSIS :/