<?php
define('NL', '<br/>');

$typeFeu = ['Dracoufeuille', 'Ponitouille', 'Sulfurette'];
$typeEau = ['Carabistouille', 'Ramoulette', 'Pykoukwak'];
$typeTerre = ['Bulbizouille', 'Empilflouille', 'Ortiche'];


// Etape 1 : on choisit un indice aléatoire pour chaque tableau :
$indexFeu = array_rand($typeFeu);
$indexEau = array_rand($typeEau);
$indexTerre = array_rand($typeTerre);

// Etape 2 incorrecte : On affiche 3 fois chaque nom (3 fois ce qu'on a fait page-6.php)
// mais on veut afficher 3 fois le premier, puis 3 fois le deuxième, puis ...
// for ($i=0 ; $i<3 ; $i++) {
//     echo $typeFeu[$indexFeu] . NL;
//     echo $typeEau[$indexEau] . NL;
//     echo $typeTerre[$indexTerre] . NL;
// }


for ($i=0 ; $i<3 ; $i++) {
    echo $typeFeu[$indexFeu] . NL;
    $indexFeu = array_rand($typeFeu);
}
for ($i=0 ; $i<3 ; $i++) {
    echo $typeEau[$indexEau] . NL;
    $indexEau = array_rand($typeEau);
}
for ($i=0 ; $i<3 ; $i++) {
    echo $typeTerre[$indexTerre] . NL;
    $indexTerre = array_rand($typeTerre);
}
